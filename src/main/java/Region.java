import javax.swing.table.TableRowSorter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public class Region extends AdministrativeComponent {
    public Region() {
        super();
    }

    public void addTerritory(Territory territory) {
        this.components.add(territory);
    }

    @Override
    public int getPopulation() {
        return super.getPopulation();
    }

    @Override
    public int getArea() {
        return super.getArea();
    }

    @Override
    public int getBudget() {
        return super.getBudget();
    }

    @Override
    public Region copy() {
        Region copy = new Region();
        for (AdministrativeComponent component: components) {
            copy.addComponent(component.copy());
        }
        return copy;
    }
}
