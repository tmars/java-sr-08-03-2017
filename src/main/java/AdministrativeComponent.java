import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public abstract class AdministrativeComponent implements IComponent, IPrototype<AdministrativeComponent> {
    protected List<AdministrativeComponent> components;

    public AdministrativeComponent() {
        this.components = new ArrayList<>();
    }

    @Override
    public int getPopulation() {
        return components
                .stream()
                .mapToInt(c -> c.getPopulation())
                .reduce(0, (a, b) -> (a + b));
    }

    @Override
    public int getArea() {
        return  components
                .stream()
                .mapToInt(c -> c.getArea())
                .reduce(0, (a, b) -> (a + b));
    }

    @Override
    public int getBudget() {
        return components
                .stream()
                .mapToInt(c -> c.getBudget())
                .reduce(0, (a, b) -> (a + b));
    }

    public void addComponent(AdministrativeComponent component) {
        components.add(component);
    }
}
