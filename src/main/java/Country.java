import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public class Country extends AdministrativeComponent {
    public Country() {
        super();
    }

    public void addRegion(Region region) {
        this.components.add(region);
    }

    @Override
    public int getPopulation() {
        return super.getPopulation();
    }

    @Override
    public int getArea() {
        return super.getArea();
    }

    @Override
    public int getBudget() {
        return super.getBudget();
    }

    @Override
    public Country copy() {
        Country copy = new Country();
        for (AdministrativeComponent component: components) {
            copy.addComponent(component.copy());
        }
        return copy;
    }
}
