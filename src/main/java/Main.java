/**
 * Created by Марсель on 08.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        Quarter quarter1 = new Quarter(300);
        quarter1.addHouse(new House(2));
        quarter1.addHouse(new House(5));
        quarter1.addHouse(new House(5));

        Quarter quarter2 = new Quarter(500);
        quarter2.addHouse(new House(3));
        quarter2.addHouse(new House(4));
        quarter2.addHouse(new House(6));

        Distinct distinct = new Distinct(55_000_000);
        distinct.addQuarter(quarter1);
        distinct.addQuarter(quarter2);

        City city = new City();
        city.addDistinct(distinct);
        city.addDistinct(distinct.copy());

        Territory territory = new Territory();
        territory.addCity(city);
        territory.addCity(city.copy());

        Region region = new Region();
        region.addTerritory(territory);
        region.addTerritory(territory.copy());

        Country country = new Country();
        country.addRegion(region);

        System.out.println("Площадь = " + country.getArea());
        System.out.println("Население = " + country.getPopulation());
        System.out.println("Бюджет = " + country.getBudget());
    }
}
