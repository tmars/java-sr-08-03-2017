import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public class Territory extends AdministrativeComponent {
    public Territory() {
        super();
    }

    public void addCity(City city) {
        this.components.add(city);
    }

    @Override
    public int getPopulation() {
        return super.getPopulation();
    }

    @Override
    public int getArea() {
        return super.getArea();
    }

    @Override
    public int getBudget() {
        return super.getBudget();
    }

    @Override
    public Territory copy() {
        Territory copy = new Territory();
        for (AdministrativeComponent component: components) {
            copy.addComponent(component.copy());
        }
        return copy;
    }
}
