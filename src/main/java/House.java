/**
 * Created by Марсель on 08.03.2017.
 */
public class House extends AdministrativeComponent {
    private int population;

    public House(int population) {
        super();
        this.population = population;
    }

    @Override
    public int getPopulation() {
        return population;
    }

    @Override
    public int getArea() {
        return 0;
    }

    @Override
    public int getBudget() {
        return 0;
    }

    @Override
    public House copy() {
        return new House(population);
    }
}
