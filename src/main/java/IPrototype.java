/**
 * Created by Марсель on 08.03.2017.
 */
public interface IPrototype<T> {
    T copy();
}
