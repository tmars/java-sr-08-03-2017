/**
 * Created by Марсель on 08.03.2017.
 */
public interface IComponent {
    int getPopulation();
    int getArea();
    int getBudget();
}
