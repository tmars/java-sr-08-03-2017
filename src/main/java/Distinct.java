import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public class Distinct extends AdministrativeComponent {
    private int budget;

    public Distinct(int budget) {
        super();
        this.budget = budget;
    }

    public void addQuarter(Quarter quarter) {
        this.components.add(quarter);
    }

    @Override
    public int getPopulation() {
        return super.getPopulation();
    }

    @Override
    public int getArea() {
        return super.getArea();
    }

    @Override
    public int getBudget() {
        return budget + super.getBudget();
    }

    @Override
    public Distinct copy() {
        Distinct copy = new Distinct(budget);
        for (AdministrativeComponent component: components) {
            copy.addComponent(component.copy());
        }
        return copy;
    }
}
