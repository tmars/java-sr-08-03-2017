import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public class Quarter extends AdministrativeComponent {
    private int area;

    public Quarter(int area) {
        super();
        this.area = area;
    }

    public void addHouse(House house) {
        this.components.add(house);
    }

    @Override
    public int getPopulation() {
        return super.getPopulation();
    }

    @Override
    public int getArea() {
        return area+super.getPopulation();
    }

    @Override
    public int getBudget() {
        return super.getPopulation();
    }


    @Override
    public Quarter copy() {
        Quarter copy = new Quarter(area);
        for (AdministrativeComponent component: components) {
            copy.addComponent(component.copy());
        }
        return copy;
    }
}
