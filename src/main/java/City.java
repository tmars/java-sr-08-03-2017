import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 08.03.2017.
 */
public class City extends AdministrativeComponent {
    public City() {
        super();
    }

    public void addDistinct(Distinct distinct) {
        this.components.add(distinct);
    }

    @Override
    public int getPopulation() {
        return super.getPopulation();
    }

    @Override
    public int getArea() {
        return super.getArea();
    }

    @Override
    public int getBudget() {
        return super.getBudget();
    }

    @Override
    public City copy() {
        City copy = new City();
        for (AdministrativeComponent component: components) {
            copy.addComponent(component.copy());
        }
        return copy;
    }
}
